# Group_K

Advanced Programming - Group Project: "Bike Sharing Analysis"

This project is the final submission of the two-phased hackathon aimed at gaining insights into the mobility of bycicle users.
Having completed both phases, the project team has successfully created a python Class named 'BikeSharingAnalysis'. 

The Class is comprised of six methods and one instance attribute:
1. download_zip(self): downloads a zip file containing the dataset.  
2. zipped_csv_to_attribute(self): reads the downloaded zip csv file into a pandas dataframe.
3. correlation_matrix(self): creates a correlation matrix of the columns month, humidity, weather situation, temperature, windspeed, and total number of bike rentals.
4. find_your_week(self): provides an input option to select a week from 0-102 and returns the plot of instances and total count for that week.
5. rentals_by_month(self): Creates a barchart plotting the total average rentals of the years by month.
6. forecast(self, month): Returns a plot with the average hourly rentals per weekday of selected month along with the variance of one standard deviation.

The instance attribute (_self_.hourly) is a DataFrame with hourly aggregated data on the bycicle usage.

To ensure a smooth execution of the analysis, we recommend setting up a virtual environment with the configurations given in the .yml file. 
For Windows users, we provide the file bikesharing.yml, for Mac users, we provide the file bikesharing_mac.yml. 
To create the virtual environment, open the anaconda prompt and set the directory to the downloaded .yml file location. 
Then, use the command "conda env create -f bikesharing(_mac).yml"  to set up the virtual environment adequate to run the analysis. 

Group members: 
Anna-Sophia Schepp - 45142@novasbe.pt 
Janina Evers - 44935@novasbe.pt 
Elisa Ritter - 44220@novasbe.pt 
Matteo Senoner - 44355@novasbe.pt 
Lukas Berndt - 45636@novasbe.pt
