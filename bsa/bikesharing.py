"""
This module offers a Class with different methods to download the Bike Sharing data
and to perform different analyses on that dataset.
"""
from urllib.request import urlretrieve
import os
import zipfile
import datetime
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

class BikeSharingAnalysis:
    """
    This class includes 4 Methods in order to download & unpack Bike Sharing data,
    as well as to analyze the dataset.

    Attributes
    ----------
    hourly: DataFrame
        DataFrame with hourly aggregations

    Methods
---------------
    __init__(self, link:string, output_file:string, source:string, input_file:string)
        Initializes instance and introduces required information for further methods.
    download_zip()
        Downloads Bike Sharing Dataset as zip file from URL into the downloads folder.
    zipped_csv_to_attribute()
        Reads and converts zipped csv file to pandas dataframe, becoming a class attribute.
    correlation_matrix()
        Creates a correlation matrix of the columns month, humidity, weather situation, temperature,
        windspeed, and total number of bike rentals.
    find_your_week()
        Provides an input option to select a week from 0-102 and returns the plot of instances
        and total count for that week.
    rentals_by_month()
        Creates a barchart plotting the total average rentals of the years by month.
    forecast(month:int)
        Returns average hourly rentals per weekday of selected month and variance of
        one standard deviation.
    """

    def __init__(self,
        link:str="https://archive.ics.uci.edu/ml/machine-learning-databases/00275/Bike-Sharing-Dataset.zip",
        output_file:str="bikesharing.zip",
        target:str="../downloads/",
        source:str="../downloads/bikesharing.zip",
        input_file:str= "hour.csv"):
        """
        Initializes instance and introduces required input information for download
        and DataFrame creation.

        Parameters
        ---------------
        link: string
            The link the zip file is downloaded from.
        output_file: string
            The filename the zip file should be saved as.
        target: string
            The path the zip file should be saved in.
        source: string
            The path the zip file should be read from.
        input_file: string
            The name of the zipped csv file that should be converted into a DataFrame.

        Returns
        ---------------
        Nothing.

        Example
        ---------------
        >>> bike = BikeSharingAnalysis()
        """
        self.link=link
        self.output_file=output_file
        self.target=target
        self.source=source
        self.input_file=input_file

    def download_zip(self):
        """
        Downloads a zip file from an URL into your hard drive.

        Returns
        ---------------
        Nothing.

        Example
        ---------------
        >>> bike.download_zip()
        """
        # If file doesn't exist, download it. Else, print a warning message.
        fullfilename = os.path.join(self.target, self.output_file)
        if not os.path.exists(fullfilename):
            urlretrieve(self.link, filename=fullfilename)
        else:
            print("File already exists!")

    def zipped_csv_to_attribute(self):
        """
        Reads a zipped csv file from a given path into a pandas dataframe becoming class attribute.

        Returns
        ---------------
        Nothing.

        Example
        ---------------
        >>> bike.zipped_csv_to_attribute()
        """
        # zip_csv becomes an object to handle this particular zip file
        try:
            zip_csv = zipfile.ZipFile(self.source)
        # if try unsuccessful, i.e. file does not exist, load file through download_zip()
        except:
            self.download_zip()
            zip_csv = zipfile.ZipFile(self.source)
        finally:
            hour = pd.read_csv(zip_csv.open(self.input_file))
            hourtime = hour.set_index(hour.columns[1])
            hourtime.index = pd.to_datetime(hourtime.index)
            self.hourly = hourtime

    def correlation_matrix(self):
        """
        Creates a correlation matrix of the columns month, humidity, weather situation, temperature,
        windspeed, and total number of bike rentals.

        Returns
        ---------------
        Correlation matrix.

        Example
        ---------------
        >>> bike.zipped_csv_to_attribute()
        """
        try:
            df_corr= self.hourly[['mnth', 'hum','weathersit',
                                                 'temp','windspeed','cnt']]
        except:
            self.zipped_csv_to_attribute()
            df_corr= self.hourly[['mnth', 'hum','weathersit',
                                                 'temp','windspeed','cnt']]
        finally:
            df_corr=df_corr.rename(columns={'mnth':'month', 'hum':'humidity',
                                            'weathersit':'weather_sit', 'temp':'temp',
                                            'cnt':'total number of rentals'})
            plt.figure(figsize=(15, 13))
            mask = np.zeros_like(df_corr.corr(), dtype=bool)
            mask[np.triu_indices_from(mask)] = True
            sns.heatmap(df_corr.corr(), mask=mask,
                        annot = True, vmin=-1, vmax=1, center= 0 , cmap= 'RdBu')
            plt.title('Correlation Matrix', fontsize=16)

    def find_your_week(self):
        """
        Provides an input option to select a week from 0-102 and returns the
        plot of instances and total count for that week.

        Returns
        ---------------
        Plot total number of ride counts for that week.

        Example
        ---------------
        >>> bike.zipped_csv_to_attribute()
        """
        try:
            dataframe = self.hourly
            mask = dataframe[(dataframe.index.to_series().dt.dayofweek) == 0].first_valid_index()
            dataframe = dataframe[mask:]
        except:
            self.zipped_csv_to_attribute()
            dataframe = self.hourly
            mask = dataframe[(dataframe.index.to_series().dt.dayofweek) == 0].first_valid_index()
            dataframe = dataframe[mask:]
        finally:
            user = int(input("Pick a week number between 0 and 102:"))
            if 0 <= user <= 102:
                base = pd.to_datetime(dataframe.index.values[0])
                start = base+datetime.timedelta(days=7*user)
                end  = base+datetime.timedelta(days=7*(user+1)-1)
                plt.figure(figsize=(10, 5))
                plt.plot((dataframe['instant'][start:end].tolist()),
                         (dataframe['cnt'][start:end].tolist()))
                plt.title(f'Plotting total number of ride counts for week {user}', fontsize=14)
                plt.ylabel('number of rides', fontsize=12)
                plt.xlabel('instances', fontsize=12)
            else:
                raise ValueError("ValueError: the allowed range lies between 0 and 102!")

    def rentals_by_month(self):
        """
        Creates a barchart plotting the total average rentals of the years by month.

        Returns
        ---------------
        Barchart

        Example
        ---------------
        >>> bike.rentals_by_month()
        """
        try:
            dataframe = self.hourly
        except:
            self.zipped_csv_to_attribute()
            dataframe = self.hourly
        finally:
            rentals_by_month = dataframe.groupby(['mnth'])[['cnt']].sum()/2
            y_values = rentals_by_month['cnt'].values
            x_values = rentals_by_month['cnt'].index.values

            fig,axes = plt.subplots(figsize=(17,8))
            axes.bar(x_values,y_values)
            month_names=[0]
            for i in x_values:
                i = str(i)
                datetime_object = datetime.datetime.strptime(i,"%m")
                month_name = datetime_object.strftime("%B")
                month_names.append(month_name)

            plt.xticks(range(len(month_names)), month_names, size='small', fontsize=10)
            plt.xlabel('Months', fontsize=14)
            plt.ylabel('Total average rentals', fontsize=14)
            plt.title('Total average rentals by month of the years 2011 & 2012', fontsize=17)
            plt.show()

    def forecast(self, month:int):
        """
        Plots a 168-hour period (Mon-Sun) corresponding to the average rental of the inputted month
        with a shaded area corresponding to an interval of [-1 std deviation, +1 std deviation].

        Parameters
        ---------------
        month: int
            integer from 1 to 12, indicating which month you want to forecast

        Returns
        ---------------
        Linechart

        Example
        ---------------
        >>> bike.forecast(1)
        """
        try:
            data = self.hourly
        except:
            self.zipped_csv_to_attribute()
            data = self.hourly
        finally:
            interval = data[(data.index.month == month)]
            month_name = interval.index[0].strftime("%B")
            interval = interval.groupby([(interval.index.to_series().dt.dayofweek + 1),
                                         interval.hr]).cnt.agg(["mean", "std"])
            interval.index = np.arange(1, len(interval)+1)

            fig, axis = plt.subplots(figsize=(20,5))
            axis.fill_between(interval.index, interval["mean"] - interval["std"],
                              interval["mean"] + interval["std"], alpha=0.4, label="1-std. dev.")
            interval["mean"].plot(ax=axis)
            axis.set_ylabel("Expected Rentals per Hour")
            plt.xticks(12*np.arange(15), 12*np.arange(15), size='small', fontsize=10)
            axis.set_xlabel("Hour of the Week")
            plt.xlim(0,len(interval)+1)
            plt.title("Expected Weekly Rentals in " + month_name)
            plt.legend()
            plt.show()
